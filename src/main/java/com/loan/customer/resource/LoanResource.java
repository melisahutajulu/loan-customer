package com.loan.customer.resource;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.loan.customer.service.LoanServiceImpl;

@RestController
@RequestMapping(value = "/loan")
public class LoanResource {

	private final LoanServiceImpl producer;
	
	@Autowired
	LoanResource(LoanServiceImpl producer) {
        this.producer = producer;
    }
	
	@PostMapping(value = "/insert")
    public void insertLoan(@RequestBody (required=true) Map<String, Integer> input) {
		
		Integer tenure 		  = input.get("tenure");
		BigDecimal ticketSize = BigDecimal.valueOf(input.get("ticketSize"));
		
        this.producer.insertLoan(tenure,ticketSize);
    }
}
