package com.loan.customer.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loan.customer.model.Loan;

@Service
public class LoanServiceImpl implements  LoanService{
	
	private static final String TOPIC = "loanTopic";

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void insertLoan(Integer tenure, BigDecimal ticketSize) {
    	
    	BigDecimal fee = ticketSize.multiply(BigDecimal.valueOf(tenure)).multiply(BigDecimal.valueOf(0.02)); 
    	BigDecimal totalLoan = ticketSize.add(fee);
    	BigDecimal installmentPerMonth = totalLoan.divide(BigDecimal.valueOf(tenure));
		String status = "SUBMITTED";
    	
    	Loan loan = new Loan();
    	loan.setTenure(tenure);
    	loan.setTicketSize(ticketSize);
    	loan.setFee(fee);
    	loan.setTotalLoan(totalLoan);
    	loan.setInstallmentPerMonth(installmentPerMonth);
    	loan.setStatus(status);
    	
    	
        this.kafkaTemplate.send(TOPIC, objToString(loan));
    }    
    
    public String objToString(Object obj) {
		String jsonStr = "";
		ObjectMapper mapper = new ObjectMapper(); 
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.setSerializationInclusion(Include.NON_DEFAULT);
		try {
			jsonStr = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} 
		return jsonStr;
	}
}
