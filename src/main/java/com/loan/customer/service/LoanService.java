package com.loan.customer.service;

import java.math.BigDecimal;

public interface LoanService {
	public void insertLoan(Integer tenure, BigDecimal ticketSize);
}
