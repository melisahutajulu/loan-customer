package com.loan.customer.model;

import java.math.BigDecimal;

public class Loan {
	Integer tenure;
	BigDecimal ticketSize;
	BigDecimal fee;
	BigDecimal totalLoan;
	BigDecimal installmentPerMonth;
	
	String status;

	public Loan() {
		super();
	}

	public Loan(Integer tenure, BigDecimal ticketSize, BigDecimal fee, BigDecimal totalLoan,
			BigDecimal installmentPerMonth, String status) {
		super();
		this.tenure = tenure;
		this.ticketSize = ticketSize;
		this.fee = fee;
		this.totalLoan = totalLoan;
		this.installmentPerMonth = installmentPerMonth;
		this.status = status;
	}

	public Integer getTenure() {
		return tenure;
	}

	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	public BigDecimal getTicketSize() {
		return ticketSize;
	}

	public void setTicketSize(BigDecimal ticketSize) {
		this.ticketSize = ticketSize;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public void setFee(BigDecimal fee) {
		this.fee = fee;
	}

	public BigDecimal getTotalLoan() {
		return totalLoan;
	}

	public void setTotalLoan(BigDecimal totalLoan) {
		this.totalLoan = totalLoan;
	}

	public BigDecimal getInstallmentPerMonth() {
		return installmentPerMonth;
	}

	public void setInstallmentPerMonth(BigDecimal installmentPerMonth) {
		this.installmentPerMonth = installmentPerMonth;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
